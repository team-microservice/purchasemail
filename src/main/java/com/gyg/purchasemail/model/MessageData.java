package com.gyg.purchasemail.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Data
public class MessageData {
    private String email;
    private double total;
    private double totalWithDiscount;
    private List<Movie> moviesList;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate date;

    @Override
    public String toString() {
        return "MessageData{" +
                "to='" + email + '\'' +
                ", total='" + total + '\'' +
                ", total con descuento='" + totalWithDiscount + '\'' +
                ", fecha='" + date + '\'' +
                ", movies='" + moviesList + '\'' +
                '}';
    }
}
