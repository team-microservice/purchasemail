package com.gyg.purchasemail.service.exceptions;


public class EmailException extends Exception {
    public EmailException(String message) {
        super(message);
    }
}