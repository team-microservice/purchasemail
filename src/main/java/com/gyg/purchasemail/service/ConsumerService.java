package com.gyg.purchasemail.service;

import com.gyg.purchasemail.model.MessageData;
import com.gyg.purchasemail.service.exceptions.EmailException;
import jakarta.mail.MessagingException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@Slf4j
@Service
public class ConsumerService {

    @Autowired
    private EmailService emailService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerService.class);

    @RabbitListener(queues = {"${rabbitmq.queue.json.name}"})
    public void receive(MessageData messageData) throws EmailException {
        LOGGER.info("Received JSON message {}", messageData.toString());

        try {
//            emailService.sendEmail(messageData);
            emailService.sendHtmlEmail(messageData);

            LOGGER.info("Send email with MAILTRAP");
        } catch (MessagingException messagingException) {
            throw new EmailException(messagingException.getMessage());
        }
    }
}
