package com.gyg.purchasemail.service;

import com.gyg.purchasemail.model.MessageData;
import com.gyg.purchasemail.model.Movie;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class EmailService {
    @Value("${spring.mail.username}")
    private String sender;

    @Autowired
    private JavaMailSender mailSender;

    public void sendEmail(MessageData messageData) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();

        message.setSubject("Compra realizada!");

        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(messageData.getEmail());

        String text = "Resumen de la compra\n\n" +
                "Se realizo la compra el dia " + messageData.getDate() + ", " +
                "abonando un monto final de $" + messageData.getTotalWithDiscount() + "\n\n" +
                "Gracias por su compra!";

        helper.setText(text);
        helper.setFrom("soporte@gyg.com");

        mailSender.send(message);
    }


    public void sendHtmlEmail(MessageData messageData) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();

        message.setFrom(new InternetAddress("soporte@gyg.com"));
        message.setRecipients(MimeMessage.RecipientType.TO, messageData.getEmail());
        message.setSubject("Compra realizada!");

//        String htmlContent = "<h1>Compraste pelicula(s)</h1>" +
//                "<p>El precio total del carrito era <strong>${total}</strong> pesos</p>" +
//                "<p>El precio final abonado en la compra fue de <strong>${totalWithDiscount}</strong> pesos</p>" +
//                "<p>El dia <strong>${date}</strong>.</p>";
        String htmlContent = readFile("src/main/resources/templates/emailTemplate.html");

        htmlContent = htmlContent.replace("${total}", String.valueOf(messageData.getTotal()));
        htmlContent = htmlContent.replace("${totalWithDiscount}", String.valueOf(messageData.getTotalWithDiscount()));
        htmlContent = htmlContent.replace("${date}", String.valueOf(messageData.getDate()));

        // Reemplazar la sección de películas
        String moviesSection = buildMoviesSection(messageData.getMoviesList());
        htmlContent = htmlContent.replace("${peliculas}", moviesSection);

        message.setContent(htmlContent, "text/html; charset=utf-8");

        mailSender.send(message);
    }

    private static String readFile(String filePath) {
        String content = "";
        try {
            // Lee todo el contenido del archivo y lo almacena en la cadena 'content'
            content = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    private String buildMoviesSection(List<Movie> moviesList) {
        // Construir la sección de películas con la lista proporcionada
        return moviesList.stream()
                .map(movie -> "<li style=\"color: #ffffff;\">" + movie.getTitle() + "</li>")
                .collect(Collectors.joining());
    }
}
