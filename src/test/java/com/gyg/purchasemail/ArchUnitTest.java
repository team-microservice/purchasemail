package com.gyg.purchasemail;


import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

public class ArchUnitTest {
    private JavaClasses jc;

    @BeforeEach
    public void init() {
        this.jc = new ClassFileImporter()
                .importPackages("..com.gyg.purchasemail");
    }

    //SERVICE
    @Test
    public void serviceShouldOnlyDependOnModel() {

        classes().that().resideInAnyPackage("..service")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..service..",
                        "..model..",
                        "java..",
                        "org.slf4j..",
                        "jakarta..",
                        "org.springframework..")
                .check(jc);
    }

    //MODEL
    @Test
    public void modelShouldNotDependOnAnyOther() {

        classes().that().resideInAnyPackage("..model")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..model..",
                        "java..",
                        "..jackson..",
                        "org.springframework..")
                .check(jc);
    }
    //CONFIG
    @Test
    public void configShouldNotDependOnAnyOther() {

        classes().that().resideInAnyPackage("..config")
                .should().onlyDependOnClassesThat()
                .resideInAnyPackage("..config..",
                        "java..",
                        "org.springframework..")
                .check(jc);
    }

}
